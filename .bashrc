# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

source /etc/profile

# Language setting
export LANGUAGE="en_GB:en"

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=4000
HISTFILESIZE=8000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# battery status
battery_status()
{
BATTERY=/sys/class/power_supply/BAT0

if $(test -e $BATTERY/charge_now)
then
		REM_CAP=`cat $BATTERY/charge_now`
		FULL_CAP=`cat $BATTERY/charge_full`
		BATSTATE=`cat $BATTERY/status`
		
		CHARGE=`echo $(( $REM_CAP * 100 / $FULL_CAP ))`
		
		BATSTT=""
		if [ "$BATSTATE" == "Charging" ]
		then
				BATSTT="+"
		fi
		
		if [ "$CHARGE" -lt "15" ]
		then
				echo -e "\e[01;31m${CHARGE}${BATSTT} "
		fi
fi
}

PS1='$(battery_status)\[\e[01;32m\]@\h \A: \[\e[01;34m\]\W \[\e[33m\]\$ \[\e[0m\]'

# If this is an xterm set the title to user@host:dir
case "$TERM" in xterm*|rxvt*)
		export TERM=xterm-256color
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias llt='ls -ltraF'
alias flgrep='grep --exclude=**/Makefile* --exclude=**/*.log --exclude=spatial* --exclude=lib* --exclude=include* --exclude=**/*.vtu -Iirsn'
alias cx1_q='ssh cx1 "qstat -a"'
alias data1='sshfs tethys:/data1 /data1'
alias data2='sshfs tethys:/data2 /data2'
alias server='sshfs server:/ /mnt/server'
alias nemacs='emacs -nw'
alias e='emacs'
alias nemo='nemo --no-desktop'
alias journey='gcloud compute ssh journey --zone europe-west1-b'
alias journeyX='gcloud compute ssh --ssh-flag="-Y" journey --zone europe-west1-b'

# alias so sudo gets user PATH variable
alias sudo='sudo env PATH=$PATH'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# add ssh-key if none exists
ssh-add -L > /dev/null
if [ $? != 0 ]; then ssh-add;  fi

# fluidity settings
export PYTHONPATH=~/Code/fluidity/trunk/python/:~/Code/fluidity/spud/python/
# module load petsc-gcc4

# other environment variables
export EDITOR=/usr/bin/emacs
export PYTHONPATH=$PYTHONPATH:~/Cloud/scripts:~/Code/adjoint-sw-sediment:~/Code/dolfin-adjoint/
export PATH=/usr/local/texlive/2013/bin/x86_64-linux:~/.local/btsync/btsync:~/.local/apache-maven-3.2.2/bin/:~/.local/bin/:$PATH
export MODULEPATH=~/.modules/modulefiles/:$MODULEPATH

# virtualenv
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Code
source /usr/local/bin/virtualenvwrapper.sh
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true

# maven
export JAVA_HOME=$HOME/.local/jdk1.8.0_11/
export ANDROID_HOME=$HOME/Code/adt-bundle-linux-x86_64-20140702/sdk/
export PATH=$JAVA_HOME/bin/:$PATH

# The next line updates PATH for the Google Cloud SDK.
source '/home/sp911/.local/google-cloud-sdk/path.bash.inc'
