(defvar *emacs-load-start* (current-time))

(push "/usr/local/bin" exec-path)
(add-to-list 'load-path "~/.emacs.d/lisp/")
(let ((default-directory "~/.emacs.d/lisp"))
      (normal-top-level-add-subdirs-to-load-path))

;; auto-saves
(setq make-backup-files nil)
(setq auto-save-default nil)

;; start-up
(setq inhibit-startup-message t)

;; terminal mouse  and keyboard settings
(require 'mouse)
(xterm-mouse-mode t)        ; enable mouse in terminal
(defun track-mouse (e))
(setq mouse-autoselect-window t)

;; aesthetics
;; (when (< emacs-major-version 24) 
;;   (require 'color-theme-convert)
;;   (add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;;   (load-theme 'blackboard t)
;;   (require 'color-theme)
;;   (setq color-theme-is-global t)
;;   (load-file "~/.emacs.d/color-themes/color-theme-blackboard.el")
;;   (when (display-graphic-p)
;;     (color-theme-blackboard))
;; )
;; (when (>= emacs-major-version 24) 
  (require 'color-theme-convert)
  (add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
;; (when (display-graphic-p)
    (load-theme 'blackboard t)
;; )
(set-default-font "Monospace 8")
(setq-default line-spacing 2)
(setq line-spacing 2)

;; terminal left-right-up-down
(global-unset-key "\M-O")
(global-set-key "\M-[1;5A"    'backward-paragraph)      ; Ctrl+right   => forward word
(global-set-key "\M-[1;5B"    'forward-paragraph)     ; Ctrl+left    => backward word
(global-set-key "\M-[1;5C"    'forward-word)      ; Ctrl+right   => forward word
(global-set-key "\M-[1;5D"    'backward-word)     ; Ctrl+left    => backward word

;; resizing windows with keypad
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)

;; expand
(global-set-key (kbd "M-/") 'dabbrev-expand)
(global-set-key (kbd "C-/") 'undo)

;; environment
(setq-default fill-column 80)
(condition-case err
    (global-linum-mode t)
  (error
   (require 'linum)
   (setq linum-format "%d ")
   (global-linum-mode 1))
)
(setq linum-format "%4d ") ;\u2502 ")			
(column-number-mode t)
(blink-cursor-mode t)
(show-paren-mode t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(set-fringe-style -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(setq-default transient-mark-mode t)
(setq x-select-enable-clipboard t)
(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)
(require 'mwheel) ;; enable wheelmouse support by default
(set-frame-parameter (selected-frame) 'alpha '(50 30))
(add-to-list 'default-frame-alist '(alpha 50 30))
(load "tellicopy.el")
(set-fill-column 100)

 ;; delete selections
(delete-selection-mode t)

;; enable disabled commands
(put 'upcase-region 'disabled nil)

;; buffers (iswitch)
(iswitchb-mode 1)
;; (iswitchb-default-keybindings)
(add-to-list 'iswitchb-buffer-ignore "^ ")
(add-to-list 'iswitchb-buffer-ignore "*scratch*")
(add-to-list 'iswitchb-buffer-ignore "*Messages*")
(add-to-list 'iswitchb-buffer-ignore "*ECB")
(add-to-list 'iswitchb-buffer-ignore "*Buffer")
(add-to-list 'iswitchb-buffer-ignore "*Completions")
(add-to-list 'iswitchb-buffer-ignore "*ftp ")
(add-to-list 'iswitchb-buffer-ignore "*bsh")
(add-to-list 'iswitchb-buffer-ignore "*jde-log")
(add-to-list 'iswitchb-buffer-ignore "^[tT][aA][gG][sS]$")

;; filetype specific 
;; Fortran
(setq auto-mode-alist (cons '("\\.F90$" . f90-mode) auto-mode-alist))
(setq f90-program-indent 2)
(setq f90-if-indent 2)
(setq f90-structure-indent 2)
(setq f90-do-indent 2)
(setq fortran-if-indent 2)
(setq fortran-structure-indent 2)
(setq fortran-do-indent 2)
(setq f90-beginning-ampersand nil)
(setq f90-type-indent 2)

;; rnc
(require 'rnc-mode)
(setq auto-mode-alist (cons '("\\.rnc\\'" . rnc-mode) auto-mode-alist))

;; rtf
(require 'rtf-mode)
(setq auto-mode-alist (cons '("\\.rtf\\'" . rtf-mode) auto-mode-alist))

;; sage
(setq auto-mode-alist (cons '("\\.sage$" . python-mode) auto-mode-alist))

;; php
(require 'php-mode)
(setq auto-mode-alist
  (append '(("\.php$" . php-mode)
            ("\.module$" . php-mode))
              auto-mode-alist))
(setq php-warned-bad-indent t)

;; pbs
(setq auto-mode-alist (cons '("\\.pbs$" . shell-script-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.sh$" . shell-script-mode) auto-mode-alist))

;; TeX
;; enable
(setq TeX-parse-self t) ; Enable parse on load.
(setq TeX-auto-save t)
;; default pdf
(setq TeX-PDF-mode t)
;; synctex
(add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
;; (setq TeX-source-correlate-start-server t)
;; turn on outlining
(defun turn-on-outline-minor-mode () (outline-minor-mode 1))
(add-hook 'LaTeX-mode-hook 'turn-on-outline-minor-mode) 
(add-hook 'latex-mode-hook 'turn-on-outline-minor-mode) 
(setq outline-minor-mode-prefix "\C-c\C-o")
;; turn on spell checker
(setq ispell-program-name "aspell") 
; could be ispell as well, depending on your preferences 
(setq ispell-dictionary "en_GB") 
; this can obviously be set to any language your spell-checking program supports
(add-hook 'LaTeX-mode-hook 'flyspell-mode) 
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)
(eval-after-load "flyspell"
'(define-key flyspell-mode-map [down-mouse-3] 'flyspell-correct-word))
;; evince
(setq TeX-output-view-style
      (quote
       (("^pdf$" "." "evince -f %o")
        ("^html?$" "." "iceweasel %o"))))
(setq TeX-source-correlate-mode t)

;; ;; matlab mode
;; (autoload 'matlab-mode "matlab" "Matlab Editing Mode" t)
;; (add-to-list
;;  'auto-mode-alist
;;  '("\\.m$" . matlab-mode))
;; (setq matlab-indent-function t)
;; (setq matlab-shell-command "matlab")

;; ;; arduino mode
;; ;; (require 'arduino-mode)
;; (setq auto-mode-alist (cons '("\\.\\(pde\\|ino\\)$" . arduino-mode) auto-mode-alist))
;; (autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)

;; less css
(require 'less-css-mode)
(setq auto-mode-alist
  (append '(("\.less$" . less-css-mode))
              auto-mode-alist))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-clean-intermediate-suffixes (quote ("\\.aux" "\\.bbl" "\\.blg" "\\.brf" "\\.fot" "\\.glo" "\\.gls" "\\.idx" "\\.ilg" "\\.ind" "\\.lof" "\\.log" "\\.lot" "\\.nav" "\\.out" "\\.snm" "\\.toc" "\\.url" "\\.synctex\\.gz" "\\.bcf" "\\.run\\.xml" "\\.mtc[0-9]*" ".maf")))
 '(LaTeX-command "lualatex")
 '(LaTeX-command-style (quote (("" "%(latex) %S%(PDFout)"))))
 '(TeX-command-list (quote (("TeX" "%(PDF)%(tex) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil (plain-tex-mode texinfo-mode ams-tex-mode) :help "Run plain TeX") ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil (latex-mode doctex-mode) :help "Run LaTeX") ("Makeinfo" "makeinfo %t" TeX-run-compile nil (texinfo-mode) :help "Run Makeinfo with Info output") ("Makeinfo HTML" "makeinfo --html %t" TeX-run-compile nil (texinfo-mode) :help "Run Makeinfo with HTML output") ("AmSTeX" "%(PDF)amstex %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil (ams-tex-mode) :help "Run AMSTeX") ("ConTeXt" "texexec --once --texutil %(execopts)%t" TeX-run-TeX nil (context-mode) :help "Run ConTeXt once") ("ConTeXt Full" "texexec %(execopts)%t" TeX-run-TeX nil (context-mode) :help "Run ConTeXt until completion") ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX") ("View" "%V" TeX-run-discard-or-function t t :help "Run Viewer") ("Print" "%p" TeX-run-command t t :help "Print the file") ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command) ("File" "%(o?)dvips %d -o %f " TeX-run-command t t :help "Generate PostScript file") ("Index" "makeindex %s" TeX-run-command nil t :help "Create index file") ("Check" "lacheck %s" TeX-run-compile nil (latex-mode) :help "Check LaTeX file for correctness") ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document") ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files") ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files") ("Other" "" TeX-run-command t t :help "Run an arbitrary command") ("XeLaTeX" "xelatex %t" TeX-run-command nil t) ("LuaLatex" "lualatex %t" TeX-run-command nil t))))
 '(custom-safe-themes (quote ("63723cbf5d668f3c33a66f6052e7ba4fff5813143f9179eb84fc83afe1ebf94e" default)))
 '(standard-indent 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; Set cursor color to white
(set-cursor-color "#ffffff") 
(set-face-background 'cursor "#ffffff")

;; spaces not tabs
(setq highlight-tabs t)
(setq highlight-trailing-whitespace t)
(setq tab-width 2)
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)

;; python tabbing
;; (add-hook 'python-mode-hook '(lambda () (setq python-indent 2)))
(put 'downcase-region 'disabled nil)
