#!/bin/bash

source ~/.bashrc

# # pulseaudio
# echo "pulseaudio setup" > .i3/startup.log
# pulseaudio --kill >> .i3/startup.log 2>&1 
# pulseaudio --start >> .i3/startup.log 2>&1 
# # gnome settings
# echo "gnome settings setup" >> .i3/startup.log
# gnome-settings-daemon >> .i3/startup.log 2>&1 &
# # GNOME keyring
# echo "gnome keyring setup" >> .i3/startup.log
# eval $(gnome-keyring-daemon --start --components=gpg,pkcs11,secrets,ssh) >> .i3/startup.log 2>&1 
# echo $SSH_AUTH_SOCK >> .i3/startup.log;
# export SSH_AUTH_SOCK >> .i3/startup.log 2>&1;
# echo $GPG_AGENT_INFO >> .i3/startup.log;
# export GPG_AGENT_INFO >> .i3/startup.log 2>&1; 
# echo $GNOME_KEYRING_CONTROL >> .i3/startup.log;
# export GNOME_KEYRING_CONTROL >> .i3/startup.log 2>&1; 
# echo $GNOME_KEYRING_PID >> .i3/startup.log;
# export GNOME_KEYRING_PID >> .i3/startup.log 2>&1;
# # cloud setup
# echo "cloud setup" >> .i3/startup.log
# ~/.local/btsync/btsync >> .i3/startup.log 2>&1 &
# exec dropbox start >> .i3/startup.log 2>&1 &
# # applets
# echo "applets setup" >> .i3/startup.log
# nm-applet >> .i3/startup.log 2>&1 &
# blueman-applet >> .i3/startup.log 2>&1 &
# # notification
# echo "notifications setup" >> .i3/startup.log
# killall notify-osd >> .i3/startup.log 2>&1 &
# dunst >> .i3/startup.log 2>&1  &

# # pulseaudio bluetooth
# pactl load-module module-bluetooth-discover >> .i3/startup.log 2>&1 
# run .desktop files
# source ~/.bashrc
timedatectl > .i3/startup.log
for i in $(ls /etc/xdg/autostart/); 
do 
    run_desktop /etc/xdg/autostart/$i i3 >> .i3/startup.log 2>&1; 
done
export SSH_AUTH_SOCK GPG_AGENT_INFO
# ~/.local/btsync/btsync >> .i3/startup.log 2>&1 &
