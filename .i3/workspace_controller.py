#!/usr/bin/python3
import subprocess
import sys
import json
import math
import os
from os.path import expanduser
from tempfile import TemporaryFile

def get_workspace():
  handle = subprocess.Popen(["i3-msg","-t","get_workspaces"], stdout=subprocess.PIPE)
  output = handle.communicate()[0]
  data = json.loads(output.decode())
  data = sorted(data, key=lambda k: k['name']) 
  for i in data:
    if(i['focused']):
      return i['name']

def get_workspaces():
  handle = subprocess.Popen(["i3-msg","-t","get_workspaces"], stdout=subprocess.PIPE)
  output = handle.communicate()[0]
  data = json.loads(output.decode())
  data = sorted(data, key=lambda k: k['name'])
  arr = []
  for i in data:
    arr.append(i['name'])
  return arr

def open_app(workspace):
  home = expanduser("~")
  cache = home+"/.cache/dmenu_run"
  check_new_programs(home, cache)
  applications = open(cache)
  dmenu_run = subprocess.Popen(["dmenu","-b"], stdout=subprocess.PIPE, stdin=applications)
  output = (dmenu_run.communicate()[0]).decode().strip()
  print("workspace "+workspace+"; exec " + output +";")
  subprocess.Popen(["i3-msg","workspace "+workspace+"; exec " + output + ";"], stdout=subprocess.PIPE)

def cmd(workspace, command):
  home = expanduser("~")
  print("workspace "+workspace+"; exec " + command +";")
  subprocess.Popen(["i3-msg","workspace "+workspace+"; exec " + command + ";"], stdout=subprocess.PIPE)

def check_new_programs(home, cachefile):
  PATH = os.environ.get('PATH')
  check = subprocess.Popen([home + "/.i3/dmenu_update"], stdout=subprocess.PIPE)
  check.communicate()

if len(sys.argv) < 1:
  print("Error not enough arguements")
else:
  command = sys.argv[1]
  # get the workspace number
  workspace_name = get_workspace()
  workspace_val = 1 # default value if name parseing fails
  try:
    workspace_val = int(workspace_name)
  except ValueError:
    pass
  # handle the commands
  if command == 'open':
    open_app(workspace_name)
  elif command == 'cmd':
    cmd(workspace_name, sys.argv[2])
